Rails.application.routes.draw do

  get 'contact/index'
  get 'checkout/index'
  get 'blog/index'
  get 'account/index'

  get 'home/index'
  get 'home/show'
  
  resources :accounts
  resources :blogs

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  
  root 'home#index'
end
